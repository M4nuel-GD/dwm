# dwm - dynamic window manager
# See LICENSE file for copyright and license details.

include config.mk

SRC = drw.c dwm.c util.c
OBJ = ${SRC:.c=.o}

all: options dwm

options:
	@echo dwm build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	${CC} -c ${CFLAGS} $<

${OBJ}: config.h config.mk

config.h:
	cp config.def.h $@

dwm: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS}

clean:
	rm -f dwm ${OBJ} dwm-${VERSION}.tar.gz

dist: clean
	mkdir -p dwm-${VERSION}
	cp -R LICENSE Makefile README config.def.h config.mk\
		dwm.1 drw.h util.h ${SRC} dwm.png transient.c dwm-${VERSION}
	tar -cf dwm-${VERSION}.tar dwm-${VERSION}
	gzip dwm-${VERSION}.tar
	rm -rf dwm-${VERSION}

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f dwm ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/dwm
	mkdir -p ${DESTDIR}${MANPREFIX}/man1
	sed "s/VERSION/${VERSION}/g" < dwm.1 > ${DESTDIR}${MANPREFIX}/man1/dwm.1
	chmod 644 ${DESTDIR}${MANPREFIX}/man1/dwm.1

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/dwm\
		${DESTDIR}${MANPREFIX}/man1/dwm.1

debian: all
	mkdir -p debian/dwm-custom/usr/bin
	mkdir -p debian/dwm-custom/usr/share/man/man1
	mkdir -p debian/dwm-custom/lib/udev/rules.d/
	cp -fr debian/DEBIAN debian/dwm-custom
	chmod 755 debian/DEBIAN debian/DEBIAN/*
	cp -f dwm debian/dwm-custom/usr/bin/
	cp -f extras/sx debian/dwm-custom/usr/bin/
	chmod 755 debian/dwm-custom/usr/bin/*
	cp -f extras/90-dwm-brightness.rules debian/dwm-custom/lib/udev/rules.d/ 
	chmod 644 debian/dwm-custom/lib/udev/rules.d/90-dwm-brightness.rules 
	# sed "s/VERSION/${VERSION}/g" < dwm.1 > debian/dwm-custom/usr/man1/dwm.1
	gzip -cf dwm.1 > debian/dwm-custom/usr/share/man/man1/dwm.1.gz
	chmod 644 debian/dwm-custom/usr/share/man/man1/dwm.1.gz
	dpkg-deb --build ./debian/dwm-custom

debian-install: debian
	dpkg -i debian/dwm-custom.deb

debian-clean:
	rm -rf debian/dwm-custom*

.PHONY: all options clean dist install uninstall
