#!/bin/sh

#https://github.com/Earnestly/sx
#This file is based on sx by Earnestly
#
#This code is used under the following license:
#
#Copyright 2017 Earnestly
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
#of the Software, and to permit persons to whom the Software is furnished to do
#so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.


# sx - start an xorg server
# requires xauth Xorg rsync /dev/urandom

export LIBVA_DRIVER_NAME='i965'
export MOZ_X11_EGL=1
export XAUTHORITY="/tmp/xauthority"
mozilla_disk="/home/$USER/.priv/.mozilla/"
mozilla_ram_disk="/tmp/.$USER.ramcache/.mozilla/"

[ ! -d "$mozilla_ram_disk" ] && mkdir -p "$mozilla_ram_disk"
rsync -au --delete $mozilla_disk $mozilla_ram_disk


cleanup() {
    if [ "$server" ] && kill -0 "$server" 2> /dev/null; then
        kill "$server"
        wait "$server"
        xorg=$?
    fi

    if ! stty "$stty"; then
        stty sane
    fi

    xauth remove :"$tty"
}

stty=$(stty -g)
tty=$(tty)
tty=${tty#/dev/tty}

touch -- "$XAUTHORITY"

trap 'cleanup; exit "${xorg:-0}"' EXIT

for signal in HUP INT QUIT TERM; do
    # shellcheck disable=SC2064
    trap "cleanup; trap - $signal EXIT; kill -s $signal $$" "$signal"
done

# Xorg will return a USR1 signal to the parent process indicating it is ready
# to accept connections if it inherited a USR1 signal with a SIG_IGN
# disposition.  Consequently a client may be started directly from a USR1
# signal handler and obviate the need to poll for server readiness.
trap 'DISPLAY=:$tty dwm & wait "$!"' USR1

xauth add :"$tty" MIT-MAGIC-COOKIE-1 "$(od -An -N16 -tx /dev/urandom | tr -d ' ')"
(trap '' USR1 && exec Xorg :"$tty" vt"$tty" -keeptty -nolisten tcp -noreset -logfile /tmp/x11-$USER -auth "$XAUTHORITY") &
server=$!
wait "$server"

rsync -au --delete $mozilla_ram_disk $mozilla_disk
